package thing;
import thing.Calculator;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.*;
import javax.servlet.http.*;
public class CelciusCalc extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Calculator calc;

    public void init() throws ServletException {
        calc = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in C: " +
                request.getParameter("isbn") + "\n" +
                "  <P>Coresponding Temperatures in F: " +
                Double.toString(calc.convert(request.getParameter("isbn"))) +
                "</BODY></HTML>");
    }


}
